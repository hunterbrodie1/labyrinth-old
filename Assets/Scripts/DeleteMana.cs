﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteMana : MonoBehaviour
{
    private Transform player;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    void Update()
    {
        if (Vector3.Distance(player.transform.position, transform.position) > 12)
        {
            Destroy(this.gameObject);
        }
    }
}
