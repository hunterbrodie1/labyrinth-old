﻿using UnityEngine;

[System.Serializable]
public class PlayerData
{
    public float x, y;
    public int mana;
    
    public PlayerData(GameObject player)
    {
        x = player.transform.position.x;
        y = player.transform.position.y;
        mana = player.GetComponent<MagicController>().GetMana();
    }
}
