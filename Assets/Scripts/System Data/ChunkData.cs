﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ChunkData
{
    public bool[,] maze;
    public int[] coord;
    public bool room;

    public ChunkData (Chunk chunk)
    {
        coord = chunk.coord;

        maze = chunk.maze;
        
        room = chunk.room;
    }
}
