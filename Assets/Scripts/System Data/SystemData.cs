﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SystemData
{
    public ChunkMapData chunkMapData;
    public PlayerData playerData;
    public EntityData entityData;

    public SystemData(ChunkMap chunkMap, GameObject player, GameObject entityParent)
    {
        chunkMapData = new ChunkMapData(chunkMap);
        playerData = new PlayerData(player);
        entityData = new EntityData(entityParent);
    }
}
