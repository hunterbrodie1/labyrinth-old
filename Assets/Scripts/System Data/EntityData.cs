﻿using System;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EntityData
{
    public List<EntityPosition> enemyPositions, potionPositions;


    public EntityData(GameObject entityParent)
    {
        enemyPositions = new List<EntityPosition>();
        potionPositions = new List<EntityPosition>();
        foreach (Transform t in entityParent.transform)
        {
            if (t.gameObject.CompareTag("Enemy"))
            {
                enemyPositions.Add(new EntityPosition { x = t.position.x, y = t.position.y });
            }
            else
            {
                potionPositions.Add(new EntityPosition { x = t.position.x, y = t.position.y });
            }
        }
    }

    [System.Serializable]
    public struct EntityPosition
    {
        public float x { get; set; }
        public float y { get; set; }
    }
}
