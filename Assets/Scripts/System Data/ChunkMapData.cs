﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ChunkMapData
{
    public ChunkData[,] chunkMap;

    public ChunkMapData(ChunkMap map)
    {
        chunkMap = new ChunkData[3, 3];
        for (int x = 0; x < 3; x++)
        {
            for (int y = 0; y < 3; y++)
            {
                chunkMap[x, y] = new ChunkData(map.getChunk(x, y));
            }
        }
    }
}
