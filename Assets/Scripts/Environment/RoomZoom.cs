﻿using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Tilemaps;

public class RoomZoom : MonoBehaviour
{
    public GameManager manager;

    private Camera cam;
    private CameraFollow follow;

    void Start()
    {
        cam = Camera.main;
        follow = GetComponent<CameraFollow>();
    }

    void Update()
    {
        if (manager.playerInRoom == true)
        {
            follow.enabled = false;

            float x = Mathf.Lerp(cam.transform.position.x, manager.getChunkMap().getMiddleChunk().coord[0] + 4 - .5f, Time.deltaTime);
            float y = Mathf.Lerp(cam.transform.position.y, manager.getChunkMap().getMiddleChunk().coord[1] + 4 - .5f, Time.deltaTime);

            //cam.transform.position = new Vector3(x, y, -5);
            //cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, 6, Time.deltaTime);
        }
        else
        {
            follow.enabled = true;

            //cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, 3, Time.deltaTime);
        }
    }
}
