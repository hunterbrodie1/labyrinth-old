﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

#region ChunkMap
public class ChunkMap
{
    private readonly bool POSITIVE = true, NORTH = true, WEST = false;

    private int chunkSize;
    private System.Random rand = new System.Random();
    private List<List<Chunk>> chunkMap = new List<List<Chunk>>();
    private Tilemap map;
    private Tile wallTile, floorTile;

    public ChunkMap(int chunkSize, Tilemap map, Tile wallTile, Tile floorTile)
    {
        this.map = map;
        this.wallTile = wallTile;
        this.floorTile = floorTile;
        this.chunkSize = chunkSize;

        for (int i = 0; i < 3; i++)
        {
            List<Chunk> tempChunkList = new List<Chunk>();
            for (int j = 0; j < 3; j++)
            {
                if (i == 1 && j == 1)
                {
                    tempChunkList.Add(new Chunk(chunkSize, i * chunkSize - (chunkSize * 3 / 2), j * chunkSize - (chunkSize * 3 / 2), rand, true));
                }
                else
                {
                    tempChunkList.Add(new Chunk(chunkSize, i * chunkSize - (chunkSize * 3 / 2), j * chunkSize - (chunkSize * 3 / 2), rand, false));
                }
            }
            chunkMap.Add(tempChunkList);
        }

        connectAllChunks();

        printChunkMap();
    }

    public ChunkMap(Tilemap map, Tile wallTile, ChunkMapData data)
    {
        this.map = map;
        this.wallTile = wallTile;
        for (int i = 0; i < 3; i++)
        {
            List<Chunk> tempChunkList = new List<Chunk>();
            for (int j = 0; j < 3; j++)
            {
                tempChunkList.Add(new Chunk(data.chunkMap[i, j]));
            }
            chunkMap.Add(tempChunkList);
        }
    }

    private void connectAllChunks()
    {
        for (int x = 0; x < 3; x++)
        {
            for (int y = 0; y < 3; y++)
            {
                if (x < 2)
                {
                    chunkMap[x][y].ConnectChunk(chunkMap[x + 1][y], WEST);
                }
                if (y < 2)
                {
                    chunkMap[x][y].ConnectChunk(chunkMap[x][y + 1], NORTH);
                }
            }
        }
    }

    public Chunk getChunk(int x, int y)
    {
        return chunkMap[x][y];
    }

    public void printChunkMap()
    {
        map.ClearAllTiles();
        for (int x = 0; x < chunkMap.Count; x++)
        {
            for (int y = 0; y < chunkMap[0].Count; y++)
            {
                chunkMap[x][y].printChunk(map, wallTile, floorTile);
            }
        }
    }

    public void printRoomObjects(GameObject lightParent, GameObject lighting)//, GameObject manaParent, GameObject mana)
    {
        foreach (Transform child in lightParent.transform)
        {
            GameObject.Destroy(child.gameObject);
        }

        for (int x = 0; x < chunkMap.Count; x++)
        {
            for (int y = 0; y < chunkMap[0].Count; y++)
            {
                if (chunkMap[x][y].room == true)
                {
                    Vector3 middle = new Vector3(chunkMap[x][y].coord[0] + chunkSize / 2, chunkMap[x][y].coord[1] + chunkSize / 2, 0);
                    GameObject.Instantiate(lighting, middle, Quaternion.identity, lightParent.transform);
                    //GameObject.Instantiate(mana, middle, Quaternion.identity, manaParent.transform);
                }
            }
        }
    }

    public Chunk getMiddleChunk()
    {
        return chunkMap[1][1];
    }

    public void insertRow(bool direction)
    {
        if (direction == POSITIVE)
        {
            for (int i = 0; i < 3; i++)
            {
                chunkMap[i].RemoveAt(0);
                chunkMap[i].Add(new Chunk(chunkSize, chunkMap[i][0].coord[0], chunkMap[i][0].coord[1] + 2 * chunkSize, rand, false));
            }
        }
        else
        {
            for (int i = 0; i < 3; i++)
            {
                chunkMap[i].RemoveAt(2);
                chunkMap[i].Insert(0, new Chunk(chunkSize, chunkMap[i][0].coord[0], chunkMap[i][0].coord[1] - chunkSize, rand, false));
            }
        }
        connectAllChunks();
        printChunkMap();
    }



    public void insertCol(bool direction)
    {
        if (direction == POSITIVE)
        {
            chunkMap.RemoveAt(0);
            List<Chunk> tempChunkList = new List<Chunk>();
            for (int i = 0; i < 3; i++)
            {
                tempChunkList.Add(new Chunk(chunkSize, chunkMap[1][1].coord[0] + chunkSize, chunkMap[0][i].coord[1], rand, false));
            }
            chunkMap.Add(tempChunkList);
        }
        else
        {
            chunkMap.RemoveAt(2);
            List<Chunk> tempChunkList = new List<Chunk>();
            for (int i = 0; i < 3; i++)
            {
                tempChunkList.Add(new Chunk(chunkSize, chunkMap[1][1].coord[0] - 2 * chunkSize, chunkMap[0][i].coord[1], rand, false));
            }
            chunkMap.Insert(0, tempChunkList);
        }
        connectAllChunks();
        printChunkMap();
    }
}
#endregion
#region Chunk
public class Chunk
{
    public bool room { get; private set; }
    public bool[,] maze { get; private set; }
    public int[] coord { get; private set; }

    private int size;

    private readonly bool WALL = false, FLOOR = true, NORTH = true;

    public Chunk(int size, int x, int y, System.Random rand, bool makeRoom)
    {
        coord = new int[2];
        coord[0] = x;
        coord[1] = y;
        this.size = size;
        maze = new bool[size, size];
        if (rand.Next(0, 50) == 0 || makeRoom == true)
        {
            GenerateRoom();
            room = true;
        }
        else
        {
            GenerateMaze(0, 0, rand);
            room = false;
        }
    }

    public Chunk(ChunkData data)
    {
        maze = data.maze;
        coord = data.coord;
        room = data.room;
    }

    private void GenerateRoom()
    {
        for (int x = 0; x < size - 1; x++)
        {
            for (int y = 0; y < size - 1; y++)
            {
                maze[x, y] = true;
            }
        }
    }

    private void GenerateMaze(int x, int y, System.Random rand)
    {
        List<int> nums = new List<int>();
        nums.Add(0);
        nums.Add(1);
        nums.Add(2);
        nums.Add(3);
        maze[x, y] = FLOOR;
        for (int i = 0; i < 4; i++)
        {
            int index = nums[rand.Next(0, nums.Count)];
            nums.Remove(index);
            if (index == 0 && x - 2 >= 0 && maze[x - 2, y] == WALL)
            {
                maze[x - 1, y] = FLOOR;
                GenerateMaze(x - 2, y, rand);
            }
            else if (index == 1 && x + 2 < maze.GetLength(0) && maze[x + 2, y] == WALL)
            {
                maze[x + 1, y] = FLOOR;
                GenerateMaze(x + 2, y, rand);
            }
            else if (index == 2 && y - 2 >= 0 && maze[x, y - 2] == WALL)
            {
                maze[x, y - 1] = FLOOR;
                GenerateMaze(x, y - 2, rand);
            }
            else if (index == 3 && y + 2 < maze.GetLength(1) && maze[x, y + 2] == WALL)
            {
                maze[x, y + 1] = FLOOR;
                GenerateMaze(x, y + 2, rand);
            }
        }
    }

    public void ConnectChunk(Chunk chunk, bool side)
    {
        if (side == NORTH)
        {
            for (int x = 0; x < size; x++)
            {
                if (x == 0 || maze[x - 1, maze.GetLength(1) - 2] != maze[x, maze.GetLength(1) - 2] || chunk.maze[x - 1, 0] != chunk.maze[x, 0])
                {
                    if (maze[x, maze.GetLength(1) - 2] == true && chunk.maze[x, 0] == true)
                    {
                        maze[x, maze.GetLength(1) - 1] = true;
                    }
                }
            }
        }
        else
        {
            for (int y = 0; y < size; y++)
            {
                if (y == 0 || maze[maze.GetLength(0) - 2, y - 1] != maze[maze.GetLength(0) - 2, y] || chunk.maze[0, y - 1] != chunk.maze[0, y])
                {
                    if (maze[maze.GetLength(0) - 2, y] == true && chunk.maze[0, y] == true)
                    {
                        maze[maze.GetLength(0) - 1, y] = true;
                    }
                }
            }
        }
    }

    public void printChunk(Tilemap map, Tile wallTile, Tile floorTile)
    {
        for (int x = 0; x < maze.GetLength(0); x++)
        {
            for (int y = 0; y < maze.GetLength(1); y++)
            {
                if (maze[x, y] == WALL)
                {
                    map.SetTile(new Vector3Int(x + coord[0], y + coord[1], 0), wallTile);
                }
                else
                {
                    map.SetTile(new Vector3Int(x + coord[0], y + coord[1], 0), floorTile);
                }
            }
        }
    }
}
#endregion
