﻿using UnityEngine;
using UnityEngine.Tilemaps;

public class GameManager : MonoBehaviour
{
    public Tilemap darkMap, blurredMap, mazeMap;
    public Tile darkTile, blurredTile, wallTile, floorTile;
    public LayerMask unwalkable;
    public GameObject player, lightParent, lightPrefab, enemyPrefab, entityParent, manaPrefab;
    [System.NonSerialized]
    public bool playerInRoom;

    //private NodeGrid nodeGrid;
    private ChunkMap chunkMap;
    private readonly bool POSITIVE = true, NEGATIVE = false;

    private readonly int HEIGHT = 25, WIDTH = 52;
    [SerializeField]
    private readonly int chunkSize = 8;


    void Start()
    {
        //nodeGrid = GetComponent<NodeGrid>();

        darkMap.size = blurredMap.size = new Vector3Int(WIDTH + 5, HEIGHT + 5, 1);
        darkMap.origin = blurredMap.origin = new Vector3Int(0 - WIDTH / 2, 0 - HEIGHT / 2, 0);

        foreach (Vector3Int p in darkMap.cellBounds.allPositionsWithin)
        {
            darkMap.SetTile(p, darkTile);
            blurredMap.SetTile(p, blurredTile);
        }

        /*if (StaticSaveSystem.load == true)
        {
            LoadGame();
            StaticSaveSystem.load = false;
        }
        else
        {*/
            chunkMap = new ChunkMap(chunkSize, mazeMap, wallTile, floorTile);
            chunkMap.printRoomObjects(lightParent, lightPrefab);//, entityParent, manaPrefab);
        //}


        //InvokeRepeating("SaveGame", 10, 10);
        InvokeRepeating("SpawnEnemy", 3, 7);
        InvokeRepeating("SpawnMana", 3, 7);
    }

    void Update()
    {
        if (player.transform.position.x > chunkMap.getMiddleChunk().coord[0] + chunkSize)
        {
            chunkMap.insertCol(POSITIVE);
            chunkMap.printRoomObjects(lightParent, lightPrefab);//, entityParent, manaPrefab);
        }
        else if (player.transform.position.x < chunkMap.getMiddleChunk().coord[0])
        {
            chunkMap.insertCol(NEGATIVE);
            chunkMap.printRoomObjects(lightParent, lightPrefab);//, entityParent, manaPrefab);
        }
        else if (player.transform.position.y > chunkMap.getMiddleChunk().coord[1] + chunkSize)
        {
            chunkMap.insertRow(POSITIVE);
            chunkMap.printRoomObjects(lightParent, lightPrefab);//, entityParent, manaPrefab);
        }
        else if (player.transform.position.y < chunkMap.getMiddleChunk().coord[1])
        {
            chunkMap.insertRow(NEGATIVE);
            chunkMap.printRoomObjects(lightParent, lightPrefab);//, entityParent, manaPrefab);
        }
        if (chunkMap.getMiddleChunk().room == true)
        {
            playerInRoom = true;
        }
        else
        {
            playerInRoom = false;
        }
    }

    public ChunkMap getChunkMap()
    {
        return chunkMap;
    }

    public void LoadMapData(ChunkMapData data)
    {
        chunkMap = new ChunkMap(mazeMap, wallTile, data);
    }

    /*private void SaveGame()
    {
        Debug.Log("Saved!");
        StaticSaveSystem.SaveSystem(chunkMap, player, entityParent);
    }

    private void LoadGame()
    {
        SystemData data = StaticSaveSystem.LoadSystem();
        if (data != null)
        {
            player.GetComponent<LoadPlayerData>().Load(data.playerData);
            LoadMapData(data.chunkMapData);
            LoadEntities(data.entityData);
        }
        else
        {
            Debug.LogError("Loading null data");
        }
    }
    
    private void LoadEntities(EntityData data)
    {
        foreach (EntityData.EntityPosition p in data.enemyPositions)
        {
            GameObject.Instantiate(enemyPrefab, new Vector3(p.x, p.y), Quaternion.identity, entityParent.transform);
        }
        foreach (EntityData.EntityPosition p in data.potionPositions)
        {
            GameObject.Instantiate(manaPrefab, new Vector3(p.x, p.y), Quaternion.identity, entityParent.transform);
        }
    }*/

    private void SpawnEnemy()
    {
        for (int x = 0; x < 500; x++)
        {
            Vector3 position = Random.insideUnitCircle.normalized * 8;
            position += player.transform.position;
            position = Vector3Int.RoundToInt(position);
            position += new Vector3(.5f, .5f);

            if (!Physics2D.OverlapCircle(position, .4f, unwalkable))
            {
                Instantiate(enemyPrefab, position, Quaternion.identity, entityParent.transform);
                break;
            }
        }
    }
    private void SpawnMana()
    {
        for (int x = 0; x < 500; x++)
        {
            Vector3 position = Random.insideUnitCircle.normalized * 8;
            position += player.transform.position;
            position = Vector3Int.RoundToInt(position);
            position += new Vector3(.5f, .5f);

            if (!Physics2D.OverlapCircle(position, .4f, unwalkable))
            {
                Instantiate(manaPrefab, position, Quaternion.identity, entityParent.transform);
                break;
            }
        }
    }
}
