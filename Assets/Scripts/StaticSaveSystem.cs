﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public static class StaticSaveSystem
{
    public static readonly string path = Path.Combine(Application.persistentDataPath, "save.sysdat");
    public static bool load = false;

    public static void SaveSystem(ChunkMap chunkMap, GameObject player, GameObject entityParent)
    {
        BinaryFormatter formatter = new BinaryFormatter();

        FileStream stream = new FileStream(path, FileMode.Create);

        formatter.Serialize(stream, new SystemData(chunkMap, player, entityParent));

        stream.Close();
    }

    public static SystemData LoadSystem()
    {

        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            SystemData data = formatter.Deserialize(stream) as SystemData;
            stream.Close();

            return data;
        }
        else
        {
            Debug.LogError("No File Located at " + path);
            return null;
        }
    }
}
