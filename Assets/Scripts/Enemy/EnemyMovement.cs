﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    private GameObject player;
    private List<Vector2> path;
    private Rigidbody2D rb;
    private Animator animator;
    private Vector2 movement;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        path = new List<Vector2>();
        player = GameObject.FindGameObjectWithTag("Player");
        animator = GetComponent<Animator>();
    }

    private void FixedUpdate()
    {
        if (Vector3.Distance(player.transform.position, transform.position) > 12)
        {
            Destroy(this.gameObject);
        }
        if (player.GetComponent<PlayerController>().envChanged == true)
        {
            PathRequestManager.RequestPath(transform.position, player.transform.position, OnPathFound);
        }

        if (path.Count > 0)
        {
            rb.velocity = 4.5f * (path[0] - rb.position).normalized;
            if (Vector3.Distance(transform.position, path[0]) < 0.1f)
            {
                path.RemoveAt(0);
            }
        }
    }

    private void Update()
    {
        animator.SetFloat("Horizontal_Movement", rb.velocity.normalized.x);
        animator.SetFloat("Vertical_Movement", rb.velocity.normalized.y);
        animator.SetFloat("Speed", rb.velocity.normalized.sqrMagnitude);
    }

    public void OnPathFound(List<Vector2> newPath, bool pathSuccessful)
    {
        if (pathSuccessful)
        {
            path = newPath;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        if (path != null)
        {
            foreach (Vector2 pos in path)
            {
                Gizmos.DrawCube(new Vector3(pos.x, pos.y, 0), Vector3.one * .5f);
            }
        }
    }
}
