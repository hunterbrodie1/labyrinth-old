﻿using System;
using System.Collections.Generic;
using UnityEngine;

#region NodeGrid
public class NodeGrid : MonoBehaviour
{
    public LayerMask unwalkable;

    private Node[,] grid;
    private readonly float nodeRadius = .4f;

    void Awake()
    {
        grid = new Node[21, 21];
        UpdateGrid();
    }
    void FixedUpdate()
    {
        if (transform.hasChanged == true)
        {
            UpdateGrid();
            transform.hasChanged = false;
        }
    }

    private void UpdateGrid()
    {
        float startX = transform.position.x - grid.GetLength(0) / 2, startY = transform.position.y - grid.GetLength(1) / 2;
        for (int x = 0; x < grid.GetLength(0); x++)
        {
            for (int y = 0; y < grid.GetLength(1); y++)
            {
                Vector2 worldPoint = new Vector2(startX + x, startY + y);
                bool walkable = !Physics2D.OverlapCircle(worldPoint, nodeRadius, unwalkable);
                grid[x, y] = new Node(walkable, worldPoint, x, y);
            }
        }
    }

    public int GetGridSize()
    {
        return grid.Length;
    }

    public List<Node> GetNodeNeighbors(Node node)
    {
        List<Node> neighbors = new List<Node>();

        /*for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                if (x == 0 && y == 0)
                {
                    continue;
                }

                int checkX = x + node.gridX, checkY = y + node.gridY;
                if (checkX > -1 && checkX < grid.GetLength(0) && checkY > -1 && checkY < grid.GetLength(1))
                {
                    neighbors.Add(grid[checkX, checkY]);
                }
            }
        }*/

        if (node.gridX - 1 > -1)
        {
            neighbors.Add(grid[node.gridX - 1, node.gridY]);
        }
        if (node.gridX + 1 < grid.GetLength(0))
        {
            neighbors.Add(grid[node.gridX + 1, node.gridY]);
        }
        if (node.gridY - 1 > -1)
        {
            neighbors.Add(grid[node.gridX, node.gridY - 1]);
        }
        if (node.gridY + 1 < grid.GetLength(1))
        {
            neighbors.Add(grid[node.gridX, node.gridY + 1]);
        }

        return neighbors;
    }

    public Node GetNodeFromPosition(Vector2 position)
    {
        if (Math.Abs(position.x - transform.position.x) < grid.GetLength(0) / 2f && Math.Abs(position.y - transform.position.y) < grid.GetLength(1) / 2f)
        {
            return grid[Mathf.RoundToInt(position.x - transform.position.x + grid.GetLength(0) / 2), Mathf.RoundToInt(position.y - transform.position.y + grid.GetLength(1) / 2)];
        }
        return null;
    }

    /*private void OnDrawGizmos()
    {
        if (transform != null && grid != null)
        {
            Gizmos.DrawWireCube(transform.position, new Vector3(grid.GetLength(0), grid.GetLength(1), 1));
            foreach (Node n in grid)
            {
                if (n.walkable)
                {
                    Gizmos.color = Color.white;
                }
                else
                {
                    Gizmos.color = Color.red;
                }

                Gizmos.DrawCube(n.worldPosition, 2 * Vector3.one * nodeRadius);
            }
        }
    }*/
}
#endregion
#region Node
public class Node : IHeapItem<Node>
{
    public bool walkable { get; private set; }
    public Vector2 worldPosition { get; private set; }

    public Node parent;
    public int gridX, gridY;

    public int gCost, hCost;
    public int fCost { get { return gCost + hCost; } }
    public int heapIndex;

    public Node(bool walkable, Vector2 worldPosition, int gridX, int gridY)
    {
        this.walkable = walkable;
        this.worldPosition = worldPosition;
        this.gridX = gridX;
        this.gridY = gridY;
    }

    public int HeapIndex
    {
        get
        {
            return heapIndex;
        }
        set
        {
            heapIndex = value;
        }
    }

    public int CompareTo(Node node)
    {
        int compare = fCost.CompareTo(node.fCost);
        if (compare == 0)
        {
            compare = hCost.CompareTo(node.hCost);
        }
        return -compare;
    }
}
#endregion
          
          