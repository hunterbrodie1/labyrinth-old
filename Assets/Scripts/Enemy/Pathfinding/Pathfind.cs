﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using Vector2 = UnityEngine.Vector2;

public class Pathfind : MonoBehaviour
{
    private PathRequestManager requestManager;
    private NodeGrid grid;

    void Awake()
    {
        grid = GetComponent<NodeGrid>();
        requestManager = GetComponent<PathRequestManager>();
    }

    public void StartFindPath(Vector2 startPos, Vector2 targetPos)
    {
        StartCoroutine(FindPath(startPos, targetPos));
    }

    IEnumerator FindPath(Vector2 startPos, Vector2 targetPos)
    {
        Node startNode = grid.GetNodeFromPosition(startPos);
        Node targetNode = grid.GetNodeFromPosition(targetPos);

        List<Vector2> waypoints = new List<Vector2>();
        bool pathSuccess = false;

        if (startNode != null)
        {
            Heap<Node> openSet = new Heap<Node>(grid.GetGridSize());
            HashSet<Node> closedSet = new HashSet<Node>();
            openSet.Add(startNode);

            while (openSet.Count > 0)
            {
                Node currentNode = openSet.RemoveFirst();
                closedSet.Add(currentNode);

                if (currentNode == targetNode)
                {
                    pathSuccess = true;
                    break;
                }

                foreach (Node neighbor in grid.GetNodeNeighbors(currentNode))
                {
                    if (!neighbor.walkable || closedSet.Contains(neighbor))
                    {
                        continue;
                    }

                    int newMovementCost = currentNode.gCost + GetNodeDistance(currentNode, neighbor);
                    if (newMovementCost < neighbor.gCost || !openSet.Contains(neighbor))
                    {
                        neighbor.gCost = newMovementCost;
                        neighbor.hCost = GetNodeDistance(neighbor, targetNode);
                        neighbor.parent = currentNode;

                        if (!openSet.Contains(neighbor))
                        {
                            openSet.Add(neighbor);
                        }
                        else
                        {
                            openSet.UpdateItem(neighbor);
                        }
                    }

                }

            }
        }

        yield return null;

        if (pathSuccess == true)
        {
            waypoints = TraceNodePath(startNode, targetNode);
        }

        requestManager.FinishedProcessingPath(waypoints, pathSuccess);
    }

    private List<Vector2> TraceNodePath(Node startNode, Node endNode)
    {
        List<Node> path = new List<Node>();
        Node currentNode = endNode;

        while (currentNode != startNode)
        {
            path.Add(currentNode);
            currentNode = currentNode.parent;
        }

        SimplifyPath(path);

        List<Vector2> waypoints = SimplifyPath(path);
        waypoints.Reverse();
        return waypoints;
    }

    private List<Vector2> SimplifyPath(List<Node> path)
    {
        List<Vector2> waypoints = new List<Vector2>();
        Vector2 directionOld = Vector2.zero;
        //Vector2 xAxis = new Vector2(1, 0);


        for (int x = 1; x < path.Count; x++)
        {
            Vector2 directionNew = new Vector2(path[x - 1].gridX - path[x].gridX, path[x - 1].gridY - path[x].gridY);

            /*if (Mathf.Abs(Vector2.Angle(xAxis, directionNew) - 90) == 45)
            {
                switch(Vector2.SignedAngle(xAxis, directionNew))
                {
                    case 45:
                        if (grid.GetNodeFromPosition(new Vector2(waypoints[x].x, waypoints[x].y - 1)).walkable == false)
                        {
                            waypoints.Insert(x, new Vector2(waypoints[x].x + 1, waypoints[x].y));
                            x += 2;
                        }
                        else if (grid.GetNodeFromPosition(new Vector2(waypoints[x].x + 1, waypoints[x].y)).walkable == false)
                        {
                            waypoints.Insert(x, new Vector2(waypoints[x].x, waypoints[x].y - 1));
                            x += 2;
                        }
                        break;
                    case -45:
                        if (grid.GetNodeFromPosition(new Vector2(waypoints[x].x, waypoints[x].y + 1)).walkable == false)
                        {
                            waypoints.Insert(x, new Vector2(waypoints[x].x + 1, waypoints[x].y));
                            x += 2;
                        }
                        else if (grid.GetNodeFromPosition(new Vector2(waypoints[x].x + 1, waypoints[x].y)).walkable == false)
                        {
                            waypoints.Insert(x, new Vector2(waypoints[x].x, waypoints[x].y + 1));
                            x += 2;
                        }
                        break;
                    case 135:
                        if (grid.GetNodeFromPosition(new Vector2(waypoints[x].x, waypoints[x].y - 1)).walkable == false)
                        {
                            waypoints.Insert(x, new Vector2(waypoints[x].x - 1, waypoints[x].y));
                            x += 2;
                        }
                        else if (grid.GetNodeFromPosition(new Vector2(waypoints[x].x - 1, waypoints[x].y)).walkable == false)
                        {
                            waypoints.Insert(x, new Vector2(waypoints[x].x, waypoints[x].y - 1));
                            x += 2;
                        }
                        break;
                    case -135:
                        if (grid.GetNodeFromPosition(new Vector2(waypoints[x].x, waypoints[x].y + 1)).walkable == false)
                        {
                            waypoints.Insert(x, new Vector2(waypoints[x].x - 1, waypoints[x].y));
                            x += 2;
                        }
                        else if (grid.GetNodeFromPosition(new Vector2(waypoints[x].x - 1, waypoints[x].y)).walkable == false)
                        {
                            waypoints.Insert(x, new Vector2(waypoints[x].x, waypoints[x].y + 1));
                            x += 2;
                        }
                        break;
                }
            }
            else */if (Vector2.Angle(directionNew, directionOld) == 0)
            {
                path.RemoveAt(x - 1);
                x--;
            }

            directionOld = directionNew;
        }

        foreach (Node n in path)
        {
            waypoints.Add(n.worldPosition);
        }

        return waypoints;
    }

    private int GetNodeDistance(Node n1, Node n2)
    {
        /*int distX = Mathf.Abs(n1.gridX - n2.gridX), distY = Mathf.Abs(n1.gridY - n2.gridY);

        if (distX > distY)
        {
            return 14 * distY + 10 * (distX - distY);
        }
        else
        {
            return 14 * distX + 10 * (distY - distX);
        }*/
        return Mathf.Abs(n1.gridX - n2.gridX) + Mathf.Abs(n1.gridY - n2.gridY);
    }

}
