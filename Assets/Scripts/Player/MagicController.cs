﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class MagicController : MonoBehaviour
{
    public Tilemap map;
    public Tile wallTile;
    public ManaBar manaBar;

    private InputMaster controls;
    private int mana;
    private readonly float xOffset = -.5f, yOffset = -.5f;

    void Awake()
    {
        controls = new InputMaster();
        controls.Player.MagicDirection.performed += mDir => CastMagic(mDir.ReadValue<Vector2>());
    }

    private void Start()
    {
        mana = 5;
        manaBar.SetMaxMana(mana);
    }

    private void OnEnable()
    {
        controls.Enable();
    }

    private void OnDisable()
    {
        controls.Disable();
    }

    private void CastMagic(Vector2 direction)
    {
        int x = (int)Mathf.RoundToInt(transform.position.x + xOffset + direction.x), y = (int)Mathf.RoundToInt(transform.position.y + yOffset + direction.y);
        if (mana > 0 && map.GetTile(new Vector3Int(x, y, 0)) == null)
        {
            mana--;
            manaBar.UpdateMana(mana);
            GetComponent<PlayerController>().envChanged = true;
            StartCoroutine(CastWall(x, y));
        }
    }

    IEnumerator CastWall(int x, int y)
    {
        map.SetTile(new Vector3Int(x, y, 0), wallTile);

        yield return new WaitForSeconds(10);

        GetComponent<PlayerController>().envChanged = true;
        map.SetTile(new Vector3Int(x, y, 0), null);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Mana"))
        {
            Destroy(collision.gameObject);
            if (mana < 5)
            {
                mana++;
                manaBar.UpdateMana(mana);
            }
        }
    }

    public void LoadMagicData(int mana)
    {
        this.mana = mana;
        manaBar.UpdateMana(mana);
    }

    public int GetMana()
    {
        return mana;
    }
}
