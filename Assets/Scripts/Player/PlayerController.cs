﻿using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public bool envChanged;

    private Rigidbody2D rb;
    private float speed = 5f, metersTravelled, highScore;
    private InputMaster controls;
    private Animator animator;
    private Vector2 movement;
    private Vector3 lastPosition;
    private TextMeshProUGUI text;
    private Vector2 lastPos;

    #region Controls
    void Awake()
    {
        controls = new InputMaster();
        controls.Player.Movement.performed += ctx => GetMoveInput(ctx.ReadValue<Vector2>());
        controls.Player.Menu.performed += men => OpenMenu();
        metersTravelled = 0;
        envChanged = true;
    }

    private void OpenMenu()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1, LoadSceneMode.Additive);
    }

    private void GetMoveInput(Vector2 movement)
    {
        this.movement = movement;
    }

    private void OnEnable()
    {
        controls.Enable();
    }

    private void OnDisable()
    {
        controls.Disable();
    }
    #endregion

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            PlayerPrefs.SetInt("score", Mathf.RoundToInt(metersTravelled));
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        text = GameObject.FindGameObjectWithTag("Score").GetComponent<TextMeshProUGUI>();
        lastPos = rb.position;
        highScore = PlayerPrefs.GetInt("score");
    }

    private void Update()
    {
        animator.SetFloat("Horizontal_Movement", rb.velocity.normalized.x);
        animator.SetFloat("Vertical_Movement", rb.velocity.normalized.y);
        animator.SetFloat("Speed", rb.velocity.normalized.sqrMagnitude);
    }

    private void FixedUpdate()
    {
        rb.velocity = speed * movement;

        if (Vector3.Distance(lastPos, transform.position) >= 1)
        {
            envChanged = true;
            metersTravelled += Vector3.Distance(transform.position, lastPosition);
            lastPosition = transform.position;
        }
        else
        {
            envChanged = false;
        }

        text.text = "High Score: " + highScore + "\nScore: " + Mathf.RoundToInt(metersTravelled);
    }

    #region Save and Load
    public Vector2 GetPosition()
    {
        return rb.position;
    }

    public void LoadPlayerData(Vector2 position)
    {
        rb.position = position;
    }
    #endregion
}
