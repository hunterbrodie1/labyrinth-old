﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadPlayerData : MonoBehaviour
{
    public void Load(PlayerData data)
    {
        transform.position = new Vector3(data.x, data.y);
        GetComponent<MagicController>().LoadMagicData(data.mana);
    }
}
