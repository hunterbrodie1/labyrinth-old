﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform player;
    public float smoothTime = 0.3F;
    private Vector3 velocity = Vector3.zero;

    void Update()
    {
        transform.position = Vector3.SmoothDamp(transform.position, new Vector3(player.transform.position.x, player.transform.position.y, -10), ref velocity, smoothTime);
    }
    /*void Update()
    {
        float x = Mathf.Lerp(transform.position.x, player.position.x, Time.deltaTime * 2f);
        float y = Mathf.Lerp(transform.position.y, player.position.y, Time.deltaTime * 2f);
        transform.position = new Vector3(x, y, transform.position.z);
    }*/
}
