﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveFloor : MonoBehaviour
{
    public Transform cameraPos;

    void Update()
    {
        transform.position = new Vector3(Mathf.RoundToInt(cameraPos.transform.position.x) - .5f, Mathf.RoundToInt(cameraPos.transform.position.y) - .5f, 0);
    }
}
