﻿using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public GameObject resume, play;

    private void Start()
    {
        if (SceneManager.sceneCount == 2 || File.Exists(StaticSaveSystem.path))
        {
            resume.SetActive(true);
            play.SetActive(false);
        }
        else
        {
            resume.SetActive(false);
            play.SetActive(true);
        }
    }

    public void PlayGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void ResumeGame()
    {
        if (SceneManager.sceneCount == 2)
        {
            SceneManager.UnloadSceneAsync("MenuScene");
        }
        else
        {
            StaticSaveSystem.load = true;
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
